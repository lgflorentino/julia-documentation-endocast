Documentation Endocast
======================

Boilerplate frame for a documentation project in [Julia](https://www.julialang.org)

# How to use

[1] Clone the repo.
[2] Update and customise the variables in `.env`
[3] Run `docker compose up -d` to have your own documentation site running locally. 

